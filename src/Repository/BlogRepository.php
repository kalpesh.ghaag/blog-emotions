<?php

namespace App\Repository;

use App\Entity\Blog;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;


class BlogRepository
{

    public function findAll(): array
    {
        $blogs = [];

        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("SELECT * FROM blog");
        $query->execute();
        
        foreach ($query->fetchAll() as $key => $line) {
            $article = new Blog();
            $article->id = $line["id"];
            $article->title = $line["title"];
            $article->description = $line["description"];
            $article->body = $line["body"];
            $article->author = $line["author"];
            $article->createdAt = $line["createdAt"];
            
            $blogs[] = $article;
        }

        return $blogs;
    }

    public function add(Blog $blog): void{
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("INSERT INTO blog(title,description,body,author,createdAt) VALUES(:title, :description, :body, :author, NOW())");
        $query->bindValue(":title", $blog->title);
        $query->bindValue(":description", $blog->description);
        $query->bindValue(":body", $blog->body);
        $query->bindValue(":author", $blog->author);
        // $query->bindValue(":createdAt", $blog->createdAt);
        
        $query->execute();

        $blog->id= $connection->lastInsertId();
    }

    

    public function find(int $id): ?Blog {
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("SELECT * FROM blog WHERE id=:id");
        $query->bindValue(":id",$id, \pdo::PARAM_INT);  
        $query->execute();      
        
        if($line = $query->fetch()){
            return $this->sqlToBlog($line);
        }
        return null;
        }
    
    private function sqlToBlog(array $line): Blog{
        $blog = new Blog();
        $blog->id = intval($line["id"]);
        $blog->title = $line["title"];
        $blog->description = $line["description"];
        $blog->body = $line["body"];
        $blog->author = $line["author"];
        $blog->createdAt = $line["createdAt"];
       
        return $blog;
    }
   
    public function update(Blog $blog):void{
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("UPDATE blog SET title=:title, description=:description, body=:body, author=:author, createdAt= NOW() WHERE id=:id");
        $query->bindValue(":id",$blog->id, \pdo::PARAM_INT); 
        $query->bindValue(":title",$blog->title, \pdo::PARAM_STR); 
        $query->bindValue(":description",$blog->description, \pdo::PARAM_STR); 
        $query->bindValue(":body",$blog->body, \pdo::PARAM_STR); 
        $query->bindValue(":author",$blog->author, \pdo::PARAM_STR); 
        // $query->bindValue(":createdAt",$blog->createdAt, \pdo::PARAM_STR); 
        
        $query->execute();  

    }
     
    public function remove(Blog $blog): void {
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("DELETE FROM blog WHERE id=:id");
        $query->bindValue(":id", $blog->id, \PDO::PARAM_INT);

        $query->execute();
    }

    public function searchValue(string $blog) {
        $blogs = [];

        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare('SELECT * FROM blog WHERE title LIKE :words ');
       
        $query->bindValue(':words', '%' . $blog . '%');
        $query->execute();
        $result = $query->fetchAll();

        foreach ($result as $value) {
            $blogs[]= $this->sqlToBlog($value);
        }
        
        return $blogs;
    }
    
}


    

