<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Blog;
use App\Form\BlogType;
use App\Repository\BlogRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;




class BlogController extends AbstractController
{

    /**
     * @Route("/", name="first_form")
     */
    public function first()
    {
        return $this->render("first-page.html.twig", []);
    }
    /**
     * @Route("/blog_form", name="form_simple")
     */
    public function blogForm(Request $request, BlogRepository $repo)
    {

        $blog = new Blog();
        $form = $this->createForm(BlogType::class, $blog);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repo->add($blog);
            // dump($blog);
            return $this->redirectToRoute('all_blogs');
        }

        // dump($form);
        $blogData = $form->getData();
        return $this->render("add-blog.html.twig", [
            'form' => $form->createView(),
            'blogData' => $blogData
        ]);
    }
    /**
     * @Route("/blogs", name="all_blogs")
     */
    public function allBlog(BlogRepository $repo)
    {
        $blogData = $repo->findAll();


        return $this->render("all-blog.html.twig", [
            'blogData' => $blogData
            // 'blogData'=>repo->findAll();
            //above code allows to do same without declaring $blogData outside.
        ]);
    }
    /**
     * @Route("/show_blog/{id}", name="show_blog")
     */
    public function showBlog(BlogRepository $repo, int $id)
    {
        return $this->render("show-blog.html.twig", [
            "blog" => $repo->find($id)
        ]);
    }
    /**
     * @Route("/remove_blog/{id}", name="remove_blog")
     */
    public function deleteBlog(BlogRepository $repo, int $id)
    {
        $blogData = $repo->find($id);
        $repo->remove($blogData);

        return $this->render("remove-blog.html.twig", [
            'blogData' => $repo->findAll()
        ]);
    }
    /**
     * @Route("/update_blog/{id}", name="update_blog")
     */
    public function updateBlog(Request $request, BlogRepository $repo, int $id)
    {
        $blogData = $repo->find($id);
        $form = $this->createForm(BlogType::class, $blogData);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repo->update($blogData);
            return $this->redirectToRoute("all_blogs");
            dump($blogData);
        }
        return $this->render("update-blog.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/search", name="search")
     */
    public function handleSearchBlog(Request $request, BlogRepository $repo)
    {

        $result = $repo->searchValue($request->get('search'));

        return $this->render("search.html.twig", [

            "search" => $result
        ]);
    }
}
