<?php

namespace App\Entity;

class Blog
{

    public $id;
    public $title;
    public $description;
    public $body;
    public $author; 
    public $createdAt;
}