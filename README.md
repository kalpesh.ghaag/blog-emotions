# Human-Emotions, are feelings.


Objective:
-----------
The objective of this project is to use the skills acquired with SYMFONY 4,PDO, PHP and Mysql.
By using this skills I have created a blog site which let us perform add, read, delete, modify and search functions.


Blog Subject: Human-Emotions:
-------------------------------

"In human beings, Emotions, are feelings which are results of the physiological events created by the autonomic nervous system, such as muscular tension, a rise in heart rate, perspiration, and dryness of the mouth, rather than being their cause."
James–Lange Theory
   

Project Unfolded:
------------------
A blog that will support a CRUD functionality, I have created,
-An Entity with it's properties, a form to provide an instance, a subsequent repository and a controller
-(.env.local) to store a Datbase Host information and added (.gitignore) to restrict it from further display on open network
-(ConnectionUtil.php) to permit access and facilitate data exchange with SQL Datbase
-(@Root) towards HTML.TWIG
-{% include %} & {% extend %} statement
-An input form using Bootstrap

Code Decoded:
---------------------
Create A Blog
<img src="/public/IMG/blogClass.png" alt="blogClass">

Search By Title
<img src="/public/IMG/search.png" alt="search">

Class Diagram & Wireframe:
---------------------
<img src="/public/IMG/blogClass.png" alt="blogClass">
<img src="/public/IMG/wireframe.png" alt="wireframe">

Blog User Interface:
---------------------
Home Page:
<img src="/public/IMG/homePage.png" alt="homePage">

All Blogs:
<img src="/public/IMG/showBlogs.png" alt="showBlogs">

Read Blog:
<img src="/public/IMG/readBlog.png" alt="readBlog">

   
Challenges Overcame:
---------------------
-This project helped me with the better understanding of cretaing "Relations" between different classes, such as "One To Many" "One To One" and so on.  
-Also this project cleared the importance of writting script in SQL workbench.

Versioning:
------------
2.0.0

Next Version:
--------------
In the following version, the goals are;
-Add image in SQL database, linked to each individual blog.
-Add commenteries to each blog.

Special Thanks:
----------------
To all the image sources online.

Author:
--------
Kalpesh Ghag

Licence:
---------
This project is an opensource project, it has licensed under the ISC norms.

Copyright © 2019 Kalpesh Ghag 


