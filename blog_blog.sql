-- MySQL dump 10.17  Distrib 10.3.12-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: blog
-- ------------------------------------------------------
-- Server version	10.3.12-MariaDB-1:10.3.12+maria~xenial

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `body` varchar(5000) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `createdAt` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` VALUES (41,'Joy','Feeling happy. Other words are happiness, gladness','Happiness is a feeling of pleasure and positivity. When someone feels good, proud, relieved or satisfied about something, that person is said to be \"happy\". Feeling happy may help people to relax and to smile.\r\n\r\nHappiness is usually thought of as the opposite of sadness. However, it is possible to feel both at once, often about different things, or sometimes even about the same thing.\r\n\r\nMany philosophers have said that people in the world go back and forth between times of happiness and sadness, but there is nobody who is always happy or always sad. Happiness sometimes causes people to cry when they laugh because the emotion takes control of them. Happiness is not the result of bouncing from one joy to the next; researchers find that achieving happiness typically involves times of considerable discomfort. Money is important to happiness, but only to a certain point. Money buys freedom from worry about the basics in life—housing, food, and clothing. Genetic makeup, life circumstances, achievements, marital status, social relationships, even your neighbors—all influence how happy you are. Or can be. So do individual ways of thinking and expressing feelings. Research shows that much of happiness is under personal control. Regularly indulging in small pleasures, getting absorbed in challenging activities, setting and meeting goals, maintaining close social ties, and finding purpose beyond oneself all increase life satisfaction.','KAL G','2019-03-17'),(42,'Fear','Feeling of being afraid, frightened, scared','Fear is a feeling or an emotion. A person who fears something does not want it to happen. The fear response comes from sensing danger. It leads to the fight-or-flight response. In extreme cases of fear (horror and terror) there may be a freeze response or paralysis.\r\n\r\nIn humans and animals, fear is adjusted by cognition and learning. Thus fear is rational or appropriate, or it is irrational or inappropriate. An irrational fear is called a phobia.\r\n\r\nFear is the body\'s way of protecting itself from doing things that may be dangerous. For example, if one has a fear of jumping off a cliff, he/she will not do it. This saves one from death. In this case, fear is a good thing but in others, it can be bad. An example of fear being bad is if it stops one from doing something important, like going to see a doctor.\r\n\r\nThere is only a small set of basic or innate emotions and fear is one of them.\r\nIn the past, human ancestors feared immediate danger, from volcano eruptions to hungry predators. Common fears today have more to do with the impression people make and how others\' judgments affect their self-worth, a hyper-focus on image that is only exacerbated by the rise of the internet and social media culture. Managing fears is confusing when they don\'t necessarily correlate with a clear or obvious danger. When they do—in cases of severe trauma, sexual or otherwise—finding ways to let go of fears and pursue positivity can help. Often, exposure to personal demons is the best way to move past them.','Béné','2019-03-16'),(43,'Surprise','A brief emotional state experienced as the result of an unexpected significant event.','Surprise is the sense of astonishment, wonder, or amazement that is caused by something sudden or unexpected. The experience of surprise varies with the importance of the outcome, as well as beliefs about the outcome. Some formalists have offered mathematical definitions of surprise (i.e., a comparison of Bayesian priors and posteriors), but there is little consensus about a psychological definition. Some researchers treat surprise as a cognitive assessment based on the probability of an event, whereas others treat it as an emotion, on par with happiness, sadness, anger, disgust, and fear because of its unique pattern of facial expressioSurprisens. If surprise is an emotion, it is an unusual one; it can be positive or negative, and it dramatically shapes the experience of other emotions.','KAL G','2019-03-16'),(44,'Sadness','Feeling sad. Other words are sorrow, grief','Sadness is an emotion. It is the opposite of happiness. People feel sad when something bad has happened, for example, if their mother or father has died, or if they are parted from friends. The word \"miserable\" has a similar meaning.\r\n\r\nIn big letters, SAD, or S.A.D. is short for \"Seasonal Affective Disorder\". SAD is a sickness that some people have in the winter. They feel depressed as the nights become longer and there is less daylight.','UNknown','2019-03-16'),(45,'Trust','A positive emotion; admiration is stronger; acceptance is weaker.','Trust is both and emotional and logical act. Emotionally, it is where you expose your vulnerabilities to people, but believing they will not take advantage of your openness. Logically, it is where you have assessed the probabilities of gain and loss, calculating expected utility based on hard performance data, and concluded that the person in question will behave in a predictable manner. In practice, trust is a bit of both. I trust you because I have experienced your trustworthiness and because I have faith in human nature.','Kalpesh','2019-03-16'),(46,'Disgust','Feeling something is wrong or nasty. Strong disapproval.','Disgust is one of the most underrated emotions. It affects everything from your beliefs to who you like to spend time with. \r\nDisgust is an emotion. People feel it when they see, touch, hear, or taste something that they think is nasty or repulsive. It is also caused by scorn. For example, when one finds something dirty or not fit to eat. Levels of disgust vary based on cultural, religious, and personal backgrounds/experiences. Disgust can be deliberate as someone can do something on purpose to create this emotion.','KAL G','2019-03-16'),(47,'Anticipation','In the sense of looking forward positively to something which is going to happen.','Robin Skynner considered anticipation as one of \"the mature ways of dealing with real stress.... You reduce the stress of some difficult challenge by anticipating what it will be like and preparing for how you are going to deal with it\". There is evidence that \"the use of mature defenses (sublimation, anticipation) tended to increase with age\".\r\nMore generally, anticipation is a central motivating force in everyday life — \"the normal process of imaginative anticipation of, or speculation about, the future\". To enjoy one\'s life, \"one needs a belief in Time as a promising medium to do things in; one needs to be able to suffer the pains and pleasures of anticipation and deferral\".','KAL G','2019-03-16'),(48,'Anger','Feeling angry. A stronger word for anger is rage.','Anger is a normal emotion. It involves a strong, uncomfortable and emotional response to a provocation. There is a sharp distinction between anger and aggression (verbal or physical, direct or indirect). Both influence each other. Anger can start aggression or increase its probability or intensity. However, it is not necessary nor sufficient for aggression.\r\n\r\nAnger is a bit like a pressure cooker: we can only apply pressure against our anger for a certain amount of time until it explodes.\r\n\r\nPeople show anger to others by their face, what they do with their body, not trying to understand or help other people\'s problems, and sometimes acts of aggression or force in public (e.g. punching a wall). Animals and humans might try to scare- by making loud sounds, trying to make their bodies look bigger, by showing their teeth, or by staring.\r\n\r\nWhen we face a challenge, our response may be anger or fear. It may be difficult to decide whether to face the challenge, or walk away. In animal behaviour terms, we face the choice of a fight or flight response.','KAL G','2019-03-17');
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-13 15:23:40
